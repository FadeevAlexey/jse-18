package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ISessionEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public String getToken(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password) throws Exception {

        System.out.println(login);
        System.out.println(password);

        @Nullable final Session session = serviceLocator.getSessionService().openSession(login, password);
        @Nullable final SessionDTO sessionDTO = convertToDTOSession(session);
        return cryptSession(sessionDTO);
    }

    @Override
    @WebMethod
    public void closeSession(@WebParam(name = "token") @Nullable final String token) throws Exception {
        serviceLocator.getSessionService().closeSession(decryptSession(token));
    }

    @Nullable
    public SessionDTO convertToDTOSession(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        if (session.getUser() == null) return null;
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setRole(session.getRole());
        return sessionDTO;
    }

}