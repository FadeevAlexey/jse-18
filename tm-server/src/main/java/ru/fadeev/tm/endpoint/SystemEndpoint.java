package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ISystemEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint() {
        super(null);
    }

    public SystemEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public String getHost(@WebParam(name = "token") @Nullable final String session) throws Exception {
        serviceLocator.getSessionService().checkSession(decryptSession(session), Role.ADMINISTRATOR);
        return serviceLocator.getPropertyService().getServerHost();
    }

    @NotNull
    public String getPort(@WebParam(name = "token") @Nullable final String session) throws Exception {
        serviceLocator.getSessionService().checkSession(decryptSession(session), Role.ADMINISTRATOR);
        return serviceLocator.getPropertyService().getServerPort();
    }

}