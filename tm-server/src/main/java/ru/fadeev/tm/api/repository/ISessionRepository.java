package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOne(@NotNull String id);

    void remove(@NotNull String id);

    void persist(Session session);

    void merge(@NotNull Session session);

    void removeAll();

    @Nullable
    Session findByUserId(@NotNull String userId);

    void removeSessionBySignature(@NotNull String signature);

    boolean contains(@NotNull String sessionId);

}