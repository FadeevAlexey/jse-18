package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository{

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    void remove(@NotNull String id);

    void persist(User user);

    void merge(@NotNull User user);

    void removeAll();

    boolean isLoginExist(@NotNull String login);

    @Nullable
    User findUserByLogin(@NotNull String login);

}