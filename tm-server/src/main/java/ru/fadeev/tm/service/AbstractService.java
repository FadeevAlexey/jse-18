package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final ServiceLocator serviceLocator;

    public AbstractService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public abstract List<E> findAll() throws Exception;

    @Nullable
    @Override
    public abstract E findOne(@Nullable String id) throws Exception;

    @Override
    public abstract void remove(@Nullable String id) throws Exception;

    @Override
    public abstract void persist(@Nullable E e) throws Exception;

    @Override
    public abstract void merge(@Nullable E e) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

}