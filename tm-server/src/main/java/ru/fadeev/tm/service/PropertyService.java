package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        @NotNull final String config = "/config.properties";
        final InputStream inputStream = PropertyService.class.getResourceAsStream(config);
        properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getServerPort() {
        @Nullable final String systemServerPort = System.getProperty("server.port");
        if (systemServerPort != null && !systemServerPort.isEmpty()) return systemServerPort;
        @Nullable final String serverPort = properties.getProperty("server.port");
        if (serverPort == null) return "8080";
        return serverPort;
    }

    @NotNull
    @Override
    public String getServerHost() {
        @Nullable final String systemServerHost = System.getProperty("server.host");
        if (systemServerHost != null && !systemServerHost.isEmpty()) return systemServerHost;
        @Nullable final String serverHost = properties.getProperty("server.host");
        if (serverHost == null) return "localhost";
        return serverHost;
    }

    @Override
    @Nullable
    public String getSessionSalt() {
        @Nullable final String salt = properties.getProperty("server.port");
        if (salt == null) return "";
        return salt;
    }

    @Override
    public int getSessionCycle() {
        @Nullable final String cycle = properties.getProperty("session.cycle");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return 0;
    }

    @Override
    public int getSessionLifetime() {
        @Nullable final String cycle = properties.getProperty("session.lifetime");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return Integer.MAX_VALUE;
    }

    public String getDbUrl() throws Exception {
        @Nullable final String type = properties.getProperty("db.type");
        if (type == null || type.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String host = properties.getProperty("db.host");
        if (host == null || host.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String port = properties.getProperty("db.port");
        if (port == null || port.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String name = properties.getProperty("db.name");
        if (name == null || name.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String login = properties.getProperty("db.login");
        if (login == null || login.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String password = properties.getProperty("db.password");
        if (password == null || password.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String timezone = properties.getProperty("db.timezone");
        if (timezone == null || timezone.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String dbUrl = String.format(
                "%s://%s:%s/%s?user=%s&password=%s&serverTimezone=%s",
                type, host, port, name, login, password, timezone
        );
        return dbUrl;
    }

    @NotNull
    public String getJdbcUrl() throws Exception {
        @Nullable final String type = properties.getProperty("db.type");
        if (type == null || type.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String host = properties.getProperty("db.host");
        if (host == null || host.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String port = properties.getProperty("db.port");
        if (port == null || port.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String name = properties.getProperty("db.name");
        if (name == null || name.isEmpty()) throw new Exception("Invalid config file");
        @Nullable final String timezone = properties.getProperty("db.timezone");
        @NotNull final String dbUrl = String.format(
                 "%s://%s:%s/%s",
                type, host, port, name
        );
        return dbUrl;
    }

    @NotNull
    public String getJdbcUserName() throws Exception {
        @Nullable final String login = properties.getProperty("db.login");
        if (login == null || login.isEmpty()) throw new Exception("Invalid config file");
        return login;
    }

    @NotNull
    public String getJdbcPassword() throws Exception {
        @Nullable final String password = properties.getProperty("db.password");
        if (password == null || password.isEmpty()) throw new Exception("Invalid config file");
        return password;
    }

    @NotNull
    public String getJdbcDriver() throws Exception {
        @Nullable final String driver = properties.getProperty("db.driver");
        if (driver == null || driver.isEmpty()) throw new Exception("Invalid config file");
        return driver;
    }

    @NotNull
    public String getSecretKey() throws Exception {
        @Nullable final String secretKey = properties.getProperty("token.secretkey");
        if (secretKey == null || secretKey.isEmpty()) throw new Exception("Invalid config file");
        return secretKey;
    }

    @NotNull
    public String getBindAddress() throws Exception {
        @Nullable final String bindAddress = properties.getProperty("jms.bindaddress");
        if (bindAddress == null || bindAddress.isEmpty()) throw new Exception("Invalid config file");
        return bindAddress;
    }

}