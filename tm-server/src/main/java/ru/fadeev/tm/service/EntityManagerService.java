package ru.fadeev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.service.ISqlSessionService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class EntityManagerService implements ISqlSessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    public void init() throws Exception {
        entityManagerFactory = factory();
    }

    public EntityManagerService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() throws Exception {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, serviceLocator.getPropertyService().getJdbcDriver());
        settings.put(Environment.URL, serviceLocator.getPropertyService().getJdbcUrl());
        settings.put(Environment.USER, serviceLocator.getPropertyService().getJdbcUserName());
        settings.put(Environment.PASS, serviceLocator.getPropertyService().getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
        settings.put(Environment.HBM2DDL_AUTO, "none");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, "true");
        settings.put(Environment.USE_QUERY_CACHE, "true");
        settings.put(Environment.USE_MINIMAL_PUTS, "true");
        settings.put("hibernate.cache.hazelcast.use_lite_member", "true");
        settings.put(Environment.CACHE_REGION_PREFIX, "task-manager");
        settings.put(Environment.CACHE_REGION_FACTORY, "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory");
        settings.put(Environment.CACHE_PROVIDER_CONFIG, "hazelcast.xml");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}