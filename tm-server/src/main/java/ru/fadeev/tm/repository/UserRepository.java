package ru.fadeev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserRepository implements IUserRepository {

    @NotNull
    private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        entityManager.merge(user);
    }

    @Override
    public void removeAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<User> cq = cb.createCriteriaDelete(User.class);
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> cq = cb.createQuery(User.class);
        @NotNull final Root<User> rootEntry = cq.from(User.class);
        @NotNull final CriteriaQuery<User> all = cq.select(rootEntry);
        TypedQuery<User> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<User> cq = cb.createCriteriaDelete(User.class);
        @NotNull final Root<User> rootEntry = cq.from(User.class);
        cq.where(cb.equal(rootEntry.get("id"), id));
        entityManager.createQuery(cq).executeUpdate();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> cq = cb.createQuery(User.class);
        @NotNull final Root<User> rootEntry = cq.from(User.class);
        @NotNull final CriteriaQuery<User> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("login"), login));
        TypedQuery<User> allQuery = entityManager.createQuery(query);
        return allQuery.getResultList().size() > 0;
    }

    @Nullable
    @Override
    public User findUserByLogin(@NotNull final String login) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> cq = cb.createQuery(User.class);
        @NotNull final Root<User> rootEntry = cq.from(User.class);
        @NotNull final CriteriaQuery<User> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("login"), login));
        TypedQuery<User> allQuery = entityManager.createQuery(query);
        return allQuery.getResultStream()
                .findFirst()
                .orElse(null);
    }

}
