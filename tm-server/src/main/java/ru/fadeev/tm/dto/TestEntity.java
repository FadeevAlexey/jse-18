package ru.fadeev.tm.dto;

import ru.fadeev.tm.context.Bootstrap;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class TestEntity {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

    private long lo = 100;

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        EntityManager em = bootstrap.getSqlSessionService().getEntityManager();
        em.getTransaction().begin();
        TestEntity testEntity = new TestEntity();
        em.persist(testEntity);
        em.getTransaction().commit();
        em.close();
    }

}


