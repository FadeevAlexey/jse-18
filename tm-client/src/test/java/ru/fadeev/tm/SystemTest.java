package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;
import java.util.List;

@Category(IntegrationTest.class)
public class SystemTest extends AbstractTest {

    @Test
    public void getPortTest() throws Exception {
        @NotNull final List<String> ports = Arrays.asList("7070", "8080", "9090");
        Assert.assertTrue(ports.contains(getSystemEndpoint().getPort(tokenAdmin)));
        boolean tokenTest;
        try {
            tokenTest = ports.contains(getSystemEndpoint().getPort(tokenUser));
        } catch (Exception e) {
            tokenTest = true;
        }
        Assert.assertTrue(tokenTest);
    }

    @Test
    public void getHostTest() throws Exception {
        @NotNull final String host = "localhost";
        Assert.assertEquals(host, getSystemEndpoint().getHost(tokenAdmin));
        boolean tokenTest;
        try {
            tokenTest = host.equals(getSystemEndpoint().getHost(tokenAdmin));
        } catch (Exception e) {
            tokenTest = true;
        }
        Assert.assertTrue(tokenTest);
    }

}