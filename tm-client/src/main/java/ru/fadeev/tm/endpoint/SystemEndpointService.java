package ru.fadeev.tm.endpoint;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;
import ru.fadeev.tm.api.endpoint.ISystemEndpoint;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-05-29T20:26:12.300+03:00
 * Generated source version: 3.2.7
 *
 */
@WebServiceClient(name = "SystemEndpointService",
                  wsdlLocation = "http://cluster/SystemEndpoint?wsdl",
                  targetNamespace = "http://endpoint.tm.fadeev.ru/")
public class SystemEndpointService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://endpoint.tm.fadeev.ru/", "SystemEndpointService");
    public final static QName SystemEndpointPort = new QName("http://endpoint.tm.fadeev.ru/", "SystemEndpointPort");
    static {
        URL url = null;
        try {
            url = new URL("http://cluster/SystemEndpoint?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(SystemEndpointService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://cluster/SystemEndpoint?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public SystemEndpointService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public SystemEndpointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SystemEndpointService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public SystemEndpointService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public SystemEndpointService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public SystemEndpointService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns ISystemEndpoint
     */
    @WebEndpoint(name = "SystemEndpointPort")
    public ISystemEndpoint getSystemEndpointPort() {
        return super.getPort(SystemEndpointPort, ISystemEndpoint.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ISystemEndpoint
     */
    @WebEndpoint(name = "SystemEndpointPort")
    public ISystemEndpoint getSystemEndpointPort(WebServiceFeature... features) {
        return super.getPort(SystemEndpointPort, ISystemEndpoint.class, features);
    }

}
