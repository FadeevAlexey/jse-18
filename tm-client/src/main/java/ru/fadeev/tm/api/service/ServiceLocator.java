package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.endpoint.*;

public interface ServiceLocator {

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAppStateService getAppStateService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}
