package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search for tasks by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printTaskList(getTaskList(token, searchRequest, typeSearch));
    }

    @NotNull
    private Collection<TaskDTO> getTaskList(
            @NotNull final String session,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        switch (typeSearch.toLowerCase()) {
            case "name": return taskEndpoint.searchByNameTask(session, searchRequest);
            case "description": return taskEndpoint.searchByDescriptionTask(session, searchRequest);
            case "all": {
                @NotNull final Collection<TaskDTO> searchByName =
                        taskEndpoint.searchByNameTask(session, searchRequest);
                @NotNull final Collection<TaskDTO> searchByDescription =
                        taskEndpoint.searchByDescriptionTask(session, searchRequest);
                @NotNull final Set<TaskDTO> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default: throw new IllegalSearchRequestException("invalid search type");
        }
    }

}