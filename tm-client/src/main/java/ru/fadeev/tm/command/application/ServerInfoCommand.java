package ru.fadeev.tm.command.application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.ISystemEndpoint;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;

public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "server-info";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Port and host information.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        @Nullable final ISystemEndpoint systemEndpoint = serviceLocator.getSystemEndpoint();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[SERVER-INFO]");
        terminal.println("HOST: " + systemEndpoint.getHost(token));
        terminal.println("PORT: " + systemEndpoint.getPort(token));
        terminal.println("[OK]\n");
    }

}