package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT TASKS]");
        terminal.println("ENTER PROJECT NAME");
        @Nullable final String projectId
                = projectEndpoint.findIdByNameProject(token, terminal.readString());
        if (projectId == null) throw new IllegalProjectNameException("Can't find project");
        terminal.printTaskList(taskEndpoint.findAllByProjectIdTask(token, projectId));
    }

}