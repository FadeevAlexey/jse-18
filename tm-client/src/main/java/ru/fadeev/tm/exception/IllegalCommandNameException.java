package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalCommandNameException extends RuntimeException {

    public IllegalCommandNameException(@Nullable final String message) {
        super(message);
    }

}
