package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.endpoint.*;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;
import ru.fadeev.tm.repository.AppStateRepository;
import ru.fadeev.tm.service.AppStateService;
import ru.fadeev.tm.service.TerminalService;

import java.lang.Exception;
import java.util.Set;

@Getter
@Setter
public class Bootstrap implements ServiceLocator {

    @NotNull
    final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    final ISystemEndpoint systemEndpoint = new SystemEndpointService().getSystemEndpointPort();

    @NotNull
    final IAppStateRepository appStateRepository = new AppStateRepository();

    @NotNull
    final IAppStateService appStateService = new AppStateService(appStateRepository);

    @NotNull
    final ITerminalService terminalService = new TerminalService();

    public void init() {
        initCommand();
        start();
    }

    private void initCommand() {
        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.fadeev.tm").getSubTypesOf(AbstractCommand.class);
        for (Class<? extends AbstractCommand> commandClass : classes) {
            try {
                registry(commandClass.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new CommandCorruptException();
            }
        }
    }

    private void start() {
        terminalService.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        while (!"exit".equals(command)) {
            try {
                command = getTerminalService().readString();
                execute(command);
            } catch (final Exception e) {
                terminalService.println(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription.isEmpty()) throw new CommandCorruptException();
        command.setServiceLocator(this);
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = getAppStateService().getCommand(command);
        if (abstractCommand == null) throw new IllegalCommandNameException("Wrong command name");
        abstractCommand.execute();
    }

}